﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace excel_sheet_unlock
{
    /// <summary>
    /// Interaction logic for DragAndDropView.xaml
    /// </summary>
    public partial class DragAndDropView : Window
    {
        private readonly SheetHandler sheetHandler;
        public DragAndDropView()
        {
            InitializeComponent();

            sheetHandler = new SheetHandler(this);
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            string[] droppedFiles = null;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                droppedFiles = e.Data.GetData(DataFormats.FileDrop, true) as string[];
            }

            if (droppedFiles == null || !droppedFiles.Any())
            {
                return;
            }

            Task.Run(() => sheetHandler.UnlockSheets(droppedFiles));
        }

        /// <summary>
        /// Updates text being displayed for the user as summary of handled files
        /// </summary>
        /// <param name="successfulChanges">Number of files that have been unlocked</param>
        /// <param name="errors">Number of errors encoutered while unlocking files</param>
        /// <param name="unchangedFiles">Number of files that have not been changed</param>
        internal void UpdateInfoLabel(int successfulChanges, int errors, int unchangedFiles)
        {
            Dispatcher.Invoke(() =>
            {
                this.InfoLabel.Content =
                        $"Successfully unlocked: {successfulChanges}" + Environment.NewLine +
                        $"Unchanged: {unchangedFiles}" + Environment.NewLine +
                        $"Erros: {errors}";
            });
        }

        /// <summary>
        /// Creates an error message for the user
        /// </summary>
        /// <param name="message">The content of the error message</param>
        internal void ShowErrorMessage(string message)
        {
            Dispatcher.Invoke(() =>
            {
                MessageBox.Show(message, "Error", MessageBoxButton.OK);
            });
        }
    }
}
