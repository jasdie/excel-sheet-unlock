﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.IO.Packaging;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Xml;

namespace excel_sheet_unlock
{
    /// <summary>
    /// Responsible for all functional handling of excel files, i.e. opening, unlocking and saving
    /// </summary>
    internal class SheetHandler
    {
        private readonly DragAndDropView view;

        internal SheetHandler(DragAndDropView view)
        {
            this.view = view;
        }

        /// <summary>
        /// Removes the sheet protection for the given excel files
        /// </summary>
        /// <param name="files">Collection of unfiltered files selected by user</param>
        /// <returns></returns>
        internal async Task UnlockSheets(ICollection<string> files)
        {
            var successfulChanges = 0;
            var errors = 0;

            var validFiles = files.Where(file => file.EndsWith(".xlsx"));

            foreach (var file in validFiles)
            {
                try
                {
                    var changed = false;

                    using (var package = Package.Open(file))
                    {
                        foreach (var part in package.GetParts().Where(part =>
                            Regex.IsMatch(part.Uri.ToString(), "xl\\/worksheets\\/sheet[0-9]+\\.xml")))
                        {
                            var doc = new XmlDocument();
                            using (var partStream = part.GetStream())
                            {
                                using (var writer = new StreamWriter(partStream))
                                {
                                    doc.Load(partStream);
                                    var oldContent = doc.OuterXml;
                                    var newContent = RemoveProtectionTag(doc);
                                    if (oldContent != newContent)
                                    {
                                        partStream.SetLength(0);
                                        await writer.WriteAsync(newContent);
                                        changed = true;
                                    }
                                }
                            }
                        }
                    }

                    if (changed)
                    {
                        successfulChanges++;
                    }
                }

                catch (IOException)
                {
                    this.view.ShowErrorMessage($"File {file} could not be opened. Is it opened in another program?");
                    errors++;
                }
                catch (FileFormatException)
                {
                    this.view.ShowErrorMessage($"File {file} cannot be unlocked due to a wrong file format. It is not a valid xlsx file.");
                }
            }

            this.view.UpdateInfoLabel(successfulChanges, errors, files.Count - successfulChanges - errors);
        }

        private string RemoveProtectionTag(XmlNode root)
        {
            var worksheetNode = root.SelectSingleNode("*[local-name()='worksheet']");
            var protectionNode = worksheetNode.SelectSingleNode("*[local-name()='sheetProtection']");
            if (protectionNode != null)
            {
                worksheetNode.RemoveChild(protectionNode);
            }

            return root.OuterXml;
        }
    }
}